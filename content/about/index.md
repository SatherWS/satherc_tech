---
title: 
date: 2020-05-15T04:49:11-04:00
draft: false
---
{{< raw >}}
    <div class="ab-landing"></div>
    <h1 class="ab-intro">
        About Sylla Source
    </h1>
{{< /raw >}}

## My Background
Growing up I always had the vague idea that I wanted to make things for a living. When researching career paths I discovered the main ways to become a maker was through blue collar trades, engineering or art. 

After careful consideration I've decided pursuing a computer science degree would be the best way to obtain the skills and career opportunities to earn a living making things. However I quickly learned that earning a college degree is simply a way to bypass barriers to entry for white collar career paths. 

I started this website to document the projects I've been working on and to demonstrate their value. People become engineers not only for the money, mainly because they want to build cool stuff. This desire to create can easily fade in our current education system, hopefully this website can combat that some how.