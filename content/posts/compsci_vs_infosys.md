---
title: "Why Information Systems is a Better Degree than Computer Science"
date: 2020-06-06T04:40:03-04:00
draft: true
---

As I enter my last semester of my computer science degree I find myself asking "was this the most efficient way to become a software developer?" computer science is a great degree, without it I wouldn't be a proficient coder or be as marketable for employment. 

I have three main reasons why information systems is a better degree than computer science. These are my personal opinions 
and definitely do not apply to everyone, please take this article with a grain of salt.

1. Information Systems Involves Easier Course Work

2. More Interesting and Relevant Projects

3. Information Systems is a More Well Rounded Degree

___
I am thinking this will be a good article for Sylla Source, It's kind of interesting.