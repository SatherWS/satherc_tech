---
title: "How to Build a Website Powered by Hugo and Google Firebase"
date: 2020-05-07T12:12:12-04:00
author: "satherc"
categories: ["Web Development"]
draft: false
images: ["/images/keyboard-article.png"]
featuredImage: "/images/keyboard-article.png"
tags: ["Web Development"]
---

In this article I will document some of the problems and solutions I've encountered while building this website. 
I built this website using `Hugo's LoveIt theme,` and I am hosting it using `Google Firebase.`

## Hugo Development
There are plenty of guides in existence that can walk you through how to create a basic Hugo website on your local machine.
Instead I will be walking you through the procedure I used to develop and deploy this site. 
___
### 1. Initial Configuration
Below are the Hugo CLI commands you'll need to run followed by a basic TOML file. Note line 15 of the TOML highlight,
this declaration enables the stylings of highlighted code.

___
[comment]: <> (Landing Photo by Hitarth Jadhav from Pexels)

{{< highlight bash "linenos=table,linenostart=1" >}}
# Create new website 
hugo new site website-name

# Add theme of choice, I chose the LoveIt theme
git clone https://github.com/dillonzq/LoveIt.git themes/LoveIt

# Run in development mode (shows posts).
hugo serve -D 

# Create a new post.
hugo new post/some-filename.md 
{{< / highlight >}}

{{< highlight toml "linenos=table,linenostart=1" >}}
baseURL = "example.com"
# [en, zh-cn, fr, ...] determines default content language
defaultContentLanguage = "en"

# language code
languageCode = "en"

# value displays in nav bar
title = "satherc_tech"

# theme of choice
theme = "LoveIt"

# enable styling of code highlights
pygmentsUseClasses=true

[params]
  # LoveIt theme version
  version = "0.2.X"

[menu]
  [[menu.main]]
  # other content not listed, see documentation ...
{{< / highlight >}}
___
### 2. Overwriting Defaults
To overwrite your theme's default structure you'll need to copy and paste the contents of your theme's layout directory into your projects layout directory. To edit the scss stylings you will need to create a directory named config with another directory named css, nested inside. 
___
### 3. Landing Page Design
I honestly spent way too much time trying to figure out how to set a background image. Setting a background image 
is a very basic task but, is much more difficult when you're using Hugo. 

Basically I solved this
problem by using inline css and saving all background images in /static/images. It's textbook spaghetti code but, it
gets the job done. This inline property sets an image background with an rgba overlay, perfect for landing pages.

{{< highlight html "linenos=table,linenostart=1">}}
<div style="background-image: linear-gradient(rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.55)), url('./images/landing-bg.jpg')">
    <!-- Other Content Here -->
</div>
{{< / highlight >}}
___
## Firebase Deployment
### 4. Why Firebase? 
The main reason is because its really easy to set up. I'm not sure if it's because Hugo is coded in Go lang which is a product of google as is firebase. I've tried hosting this site using both GitLab and GitHub and I found the process to be much more tedious especially if you want to use your own domain name. 

### 5. Steps to Set Up Firebase
You must have the node package manager (npm) installed in order to install the firebase command line tools.
After that enter the commands below into your terminal except for the comments of course. After you run `firebase init`
you will need to answer yes to all the questions except for the question: `configure as a single-page.` 
{{< highlight bash >}}
npm install -g firebase-tools
cd path/to/your-project
firebase login
firebase init
hugo
firebase deploy
{{< / highlight >}}

> It's that simple :point_up:
___
### 6. Push to GitLab
The documentation on Hugo's website is very helpful, however I wanted to clone my theme directly into my workspace.
My reasoning for this is to get a better understanding of how the theme works, however GitLabs does not allow you to commit nested
repositories. Hence the need for line 5 of the below commands.

{{< highlight bash "linenos=table,linenostart=1" >}}
cd your-project
git init
echo "/public" >> .gitignore
# you must add your theme as a submodule 
git submodule add https://github.com/dillonzq/LoveIt.git themes/LoveIt
git add . 
git commit -m "some message"
git remote add origin https://gitlab.com/YourUsername/your-hugo-site.git
git push -u origin master
{{< / highlight >}}
___
### 7. Deployment Script
After you've completed the above steps, I'd recommend consolidating these commands into a single shell script.
This way whenever you make changes to your site you can simply run your script to push your code to GitLab and deploy your
site to Firebase simultaneously.
> **Note:** I've created new folder for my shell script called *deploy*
{{< highlight shell "linenos=table,linenostart=1" >}}
#!/bin/sh
[[ -f saved_value ]] || echo 0 > saved_value
# variable n is increments after each script execution
n=$(< saved_value) 
echo $(( n + 1 )) > saved_value

cd .. && hugo
printf "\033[0;32mDeploying updates to Firebase...\033[0m\n" 
firebase deploy -m "firebase deployment #$n"
printf "\033[0;32mDeploying updates to GitLab...\033[0m\n" 
git add . && git commit -m "firebase deployment #$n"
git push -u origin master
{{< / highlight >}}


