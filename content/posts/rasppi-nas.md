---
title: "How to Build a Cheap NAS"
date: 2020-05-16T15:22:05-04:00
draft: false
author: satherc
categories: ["Raspberry Pi"]
tags: ["Raspberry Pi", "System Administration"]
featuredImage: "/images/rasp-nas-final.png"
---
A Network attached storage device (NAS) is simply a hard drive that is accessible from a WiFi and/or Ethernet network. In this article I will be demonstrating how I set up a 128gb NAS for under 50 dollars USD. I am also assuming you have your raspberry pi up and running with Raspbian OS. 

## Supplies
* Raspberry Pi 3 Model B Plus
* Old and/or Broken Computer
* USB to SATA hard Drive Adapter Cable

## Directions
### 1. Extract Hard Drive
For this step we will assume your old computer is a laptop, but can easily be replicated if you have an old desktop or any external hard drive. Whenever you're extracting components from a computer you need to first disconnect the battery or power supply to avoid shorting your components. 
{{< figure src="/images/remove-hd.jpg" title="Figure 1" >}}

### 2. Format Hard Drive
Once you've successfully extracted the hard drive from your old computer you'll need to format the drive in order for it to be usable in a Linux environment. **Warning:** this operation will delete the contents of your external hard drive, so be sure to back up the data.

{{< figure src="/images/hard-drive-formatting.png" title="Figure 2" >}}

### 3. Install Samba
Samba is free software that allows Windows networking protocols, such as file and printer sharing to work on Linux machines as well. To install this software open your Raspberry pi terminal either locally or remotely and enter the following commands. 
I learned how to do this myself by reading [this MagPi article](https://magpi.raspberrypi.org/articles/samba-file-server).

{{< highlight bash "linenos=table,linenostart=1" >}}
sudo apt-get update

sudo apt-get upgrade

sudo apt-get install samba samba-common-bin
{{< / highlight >}}
___
Now we will need to make a shared directory on our external hard drive. To accomplish this I ran this command: `sudo mkdir -m 1777 /media/usb/share`

Next we will need to modify our smb configuration: `sudo nano /etc/samba/smb.conf`

Copy and paste the below code to the end of the smb configuration file. To quickly move to the end of a file in the nano editor type `Alt + /`.
___
{{< highlight bash "linenos=table,linenostart=1" >}}
[share]
path = /media/usb/share                                                                                                               
comment = Share                                                                                                                       
writeable=Yes                                                                                                                         
guest ok = yes                                                                                                                        
create mask=0777                                                                                                                      
directory mask=0777                                                                                                                   
force user = pi
{{< / highlight >}}
___
## Closing Remarks
In my opinion this is a great raspberry pi/arduino starter project because its useful, easy to do and relevant in any kind of office environment. If you have any questions or comments please leave a comment below, thanks for reading.